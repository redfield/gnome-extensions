// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

const Gio = imports.gi.Gio;
const Lang = imports.lang;
const GLib = imports.gi.GLib;

const netctlIface = '\
<node>\
    <interface name="com.gitlab.redfield.netctl.wireless">\
        <method name="WirelessConnect">\
                <arg direction="in" type="s"/>\
                <arg direction="out" type="i"/>\
        </method>\
        <method name="WirelessConnectWithSaved">\
                <arg direction="in" type="s"/>\
                <arg direction="out" type="b"/>\
        </method>\
        <method name="WirelessDisconnect">\
        </method>\
        <method name="WirelessGetProperties">\
                <arg direction="out" type="s"/>\
        </method>\
        <signal name="PropertiesChanged">\
                <arg type="s" direction="out"/>\
        </signal>\
    </interface>\
</node>';

const gnomePortalHelperIface = '\
<node>\
    <interface name="org.gnome.Shell.PortalHelper">\
        <method name="Authenticate">\
          <arg name="connection" type="o" direction="in"/>\
          <arg name="url" type="s" direction="in"/>\
          <arg name="timestamp" type="u" direction="in"/>\
        </method>\
        <method name="Close">\
          <arg name="connection" type="o" direction="in"/>\
        </method>\
        <method name="Refresh">\
          <arg name="connection" type="o" direction="in"/>\
        </method>\
        <signal name="Done">\
          <arg type="o" name="connection"/>\
          <arg type="u" name="result"/>\
        </signal>\
    </interface>\
</node>';

const NetctlIfaceProxy = Gio.DBusProxy.makeProxyWrapper(netctlIface);
const PortalHelperProxy = Gio.DBusProxy.makeProxyWrapper(gnomePortalHelperIface);

var netctlDBusInterface = new Lang.Class({
    Name: "netctlDBusInterface",

    _init: function(menuObject) {
        let self = this;

        this.menu = menuObject;

        this.UPDATE_TYPES = {
            UNSPECIFIED: 0,
            STATE: 1,
            IP: 2,
            SIGNAL: 3,
            AVAILABLE_NETWORKS: 4,
            SSID: 5,
        };

        this.UPDATE_STATES = {
            UNKNOWN: 0,
            DISCONNECTED: 1,
            CONNECTED: 2,
            CONNECTING: 3,
            PORTAL: 4,
            FAILED: 5,
        };

        // The proxy will be initialized when the netctl name
        // appears on the system bus.
        this.proxy = null;

        // Watch for the netctl name on the system bus, and
        // setup handlers for when the name appears/vanishes.
        Gio.bus_watch_name(
            Gio.BusType.SYSTEM,
            'com.gitlab.redfield.netctl.wireless',
            Gio.BusNameWatcherFlags.NONE,
            () => /* Handler if the name appears on system bus */ {
                this.proxy = new NetctlIfaceProxy(
                    Gio.DBus.system,
                    'com.gitlab.redfield.netctl.wireless',
                    '/com/gitlab/redfield/netctl/wireless'
                );

                // Set the initial properties on the menu.
                this.setInitialProperties();

                // Register a handler for the PropertiesChanged signal.
                this._updateSignalId = this.proxy.connectSignal(
                    "PropertiesChanged",
                    (proxy, sender, s) => {
                        this.updateStatus(s);
                });
            },
            () => /* Handler if the name vanishes from system bus */ {
                global.log('com.gitlab.redfield.netctl.wireless vanished from the system bus');
                this.menu.failure();
            }
        );
    },

    setInitialProperties: function() {
        let props = JSON.parse(this.proxy.WirelessGetPropertiesSync());

        this.menu.netlist = props.access_points.filter(
            (ap) => { return ap.ssid != ""}
        );

        switch (props.state) {
        case this.UPDATE_STATES.CONNECTED:
            if (props.ssid) {
                this.menu.connectedTo(props.ssid);
                this.menu.updateIcon(props.signal_strength);
                break;
            }
            /* Fallthrough */
        case this.UPDATE_STATES.CONNECTING:
            this.menu.connecting();
            break;
        case this.UPDATE_STATES.DISCONNECTED:
        case this.UPDATE_STATES.FAILED:
            this.menu.disconnected();
            break;
        case this.UPDATE_STATES.PORTAL:
            this.menu.portal();
            // Launch portal login page
            this.launchPortalHelper(props.portal_url)
            break;
        default:
            this.menu.unknown();
        }
    },

    updateStatus: function (s) {
        let self = this;
        let update = JSON.parse(s);

        switch(update.type) {
        case this.UPDATE_TYPES.AVAILABLE_NETWORKS:
            this.menu.netlist = update.props.access_points
                .filter((ap) => {
                    return ap.ssid != "";
                });

            this.menu.iface = update.props.iface_name;

            if (!update.props.hasOwnProperty("state")) {
                break;
            }
        case this.UPDATE_TYPES.STATE:
            let state = update.props.state;

            switch (state) {
            case this.UPDATE_STATES.CONNECTED: //"connected":
                if (update.props.hasOwnProperty("ssid")) {
                    this.menu.connectedTo(update.props.ssid);
                }
                if (update.props.hasOwnProperty("signal_strength")) {
                    this.menu.updateIcon(update.props.signal_strength);
                }
                break;
            case this.UPDATE_STATES.DISCONNECTED: //"disconnected":
                this.menu.disconnected();
                break;
            case this.UPDATE_STATES.CONNECTING: // connecting
                this.menu.connecting();
                break;
            case this.UPDATE_STATES.PORTAL: // portal
                this.menu.portal();
                // Launch portal login page
                this.launchPortalHelper(update.props.portal_url)
                break;
            default:
                this.menu.unknown();
            }
            break;

        case this.UPDATE_TYPES.IP:
            this.menu.updateIP(update.props.ip_address);
            break;

        case this.UPDATE_TYPES.SSID:
            if (update.props.hasOwnProperty("ssid") && this.menu.wirelessConnected) {
                this.menu.connectedTo(update.props.ssid);
            }
            break;

        case this.UPDATE_TYPES.SIGNAL:
            if (update.props.hasOwnProperty("signal_strength") && this.menu.wirelessConnected) {
                this.menu.updateIcon(update.props.signal_strength);
            }
            break;
        default:
        }
    },

    launchPortalHelper: function(url) {
        // Call org.gnome.Shell.PortalHelper
        // Requires arguements: DBus object path, URL, timestamp
        let portalHelper = new PortalHelperProxy(
            Gio.DBus.session,
            'org.gnome.Shell.PortalHelper',
            '/org/gnome/Shell/PortalHelper'
        );

        // Make timestamp
        let ts = Math.round((new Date()).getTime() / 1000);

        // Make the call to org.gnome.Shell.PortalHelper.Authenticate, asynchronously
        portalHelper.AuthenticateRemote(portalHelper.g_object_path, url, ts)
    },

    disable: function() {
        if (this._updateSignalId)
            this.proxy.disconnectSignal(this._updateSignalId);
    }
})
